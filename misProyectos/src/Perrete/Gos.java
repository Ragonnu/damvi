package Perrete;

import java.util.Random;

public class Gos 
{
	private int edat;
	private double alçada;
	private String nom;
	
	public void borda() 
	{
		System.out.println("Guau Guau");
	}
	public void presentarse() 
	{
		System.out.println("Hola, me llamo "+nom+", mido "+alçada+"m y tengo "+edat+" años");
	}
	public Gos clonar(Gos nom)
	{
		return nom;
		
	}

	public Gos(int edat, double alçada, String nom)
	{
		this.edat = edat;
		this.alçada = alçada;
		this.nom = nom;
	}
	
	public Gos(Gos g)
	{
		this.edat = g.edat;
		this.alçada = g.alçada;
		this.nom = g.nom;
	}
	
	public Gos(String nom)
	{
		this.edat = 0;
		this.alçada = 0.20;
		this.nom = nom;
//		Random r = new Random();
//		this.genero = r.nextBoolean()? 'm':'f';
//		this.especie = especie;
	}
	
	
	public int getEdat() 
	{
		return edat;
	}
	public void setEdat(int edat)
	{
		this.edat = edat;
	}
	public double getAlçada()
	{
		return alçada;
	}
	public void setAlçada(double alçada
			) {
		this.alçada = alçada;
	}
	public String getNom() 
	{
		return nom;
	}
	public void setNom(String nom)
	{
		this.nom = nom;
	}
	
	
	
}
