package Perrete;

public class ProgGos
{

	public static void main(String[] args) 
	{
		Gos perra = new Gos(7, 0.46, "dina");
		
		perra.borda();
		
		perra.setEdat(9);
		perra.presentarse();
		
		Gos copiaPerra = new Gos(perra);
		
		copiaPerra.presentarse();
		
		Gos nado = new Gos("Paco");
		Gos nado2 = nado.clonar(nado);;
		
		nado.presentarse();
		nado2.presentarse();
	}

}
