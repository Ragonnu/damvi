package UF2;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class PalabrasInvertidasTest
{
	@Test
	public void test()
	{
		String entrada = "rafa troll coche";
		String salida = "coche troll rafa";
		assertEquals(salida, PalabrasInvertidas.ProcesarYVolcar(entrada));
		
		String entrada2 = "rafa troll coche Davilillo alcantarilla";
		String salida2 = "alcantarilla Davilillo coche troll rafa";
		assertEquals(salida2, PalabrasInvertidas.ProcesarYVolcar(entrada2));
		
		String entrada3 = "rafa";
		String salida3 = "rafa";
		assertEquals(salida3, PalabrasInvertidas.ProcesarYVolcar(entrada3));
		
		String entrada4 = "casa caballo";
		String salida4= "caballo casa";
		assertEquals(salida4, PalabrasInvertidas.ProcesarYVolcar(entrada4));
		
		String entrada5 = "";
		String salida5 = "";
		assertEquals(salida5, PalabrasInvertidas.ProcesarYVolcar(entrada5));
		
		String entrada6 = "ra fa tro ll co che";
		String salida6= "che co ll tro fa ra";
		assertEquals(salida6, PalabrasInvertidas.ProcesarYVolcar(entrada6));
		
		String entrada7 = "Marc fue corriendo al instituto y por ir corriendo fue atropellado";
		String salida7 = "atropellado fue corriendo ir por y instituto al corriendo fue Marc";
		assertEquals(salida7, PalabrasInvertidas.ProcesarYVolcar(entrada7));		
	}
}
