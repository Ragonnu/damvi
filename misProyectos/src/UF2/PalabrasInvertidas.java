package UF2;

import java.util.Scanner;

public class PalabrasInvertidas 
{

	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) 
	{
		  int cantidad = sc.nextInt();
		  sc.nextLine();
		  
	        for (int i = 1; i <= cantidad; i++)
	        {
	            String texto = sc.nextLine();
	            System.out.println(ProcesarYVolcar(texto));
	        }
	        
	}
	static String ProcesarYVolcar(String texto) 
	{
		  // Descompongo en un array
        String[] palabras = texto.split(" ");
        String resultado = "";
 
        // Y recorro el array al revés, mostrando datos
       
        for (int  j = palabras.length; j > 0; j--)
        {
        	//System.out.print(palabras[j-1]);
        	resultado = resultado + palabras[j-1];
            if (j>1)  // Espacio separador, excepto tras la última
            
            //System.out.print(" ");
            resultado = resultado + " ";
        }
 
        //System.out.println("\n");
        return resultado;
	}
	

}
