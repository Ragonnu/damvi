package ExamenMatricesP;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

public class Ejercicio1 
{

	public static void main(String[] args) 
	{
		System.out.println("Menú");
		System.out.println("1-Juego nuevo");
		System.out.println("2-Rellenar Sudoku");
		System.out.println("3-Ver Sudoku");
		System.out.println("4-Premio");
		System.out.println("0-Salir");
		Scanner sc = new Scanner(System.in);
		
		int volumen = 1;
		boolean salir = false;
		int [][] matriz = new int [3][3];
		
		while (!salir)
		{
			int n = sc.nextInt();
			
			switch (n) 
			{
			case 1:
				for (int f = 0; f < matriz.length; f++) 
				{
					for (int c = 0; c < matriz[0].length; c++) 
					{
						matriz[f][c] = 0;
					}
				}
				break;
			case 2:
				TreeSet<Integer> tree = new TreeSet<Integer>();
				
				for (int i = 0; i < 9; i++) 
				{
					tree.add(sc.nextInt());
				}
				

				if (tree.size() == 9) 
				{
					ArrayList<Integer> l3 = new ArrayList<Integer>();
					l3.addAll(tree);
					int acc = 0;
					for (int f = 0; f < matriz.length; f++) 
					{
						for (int c = 0; c < matriz[0].length; c++) 
						{
							matriz[f][c] = l3.get(acc);
							acc++;
						}
					} 
				}
				else
				{
					System.out.println("ERROR! Hay valores repetidos, empieza de nuevo.");
					salir = true;				
				}
				break;
			case 3:
				for (int f = 0; f < matriz.length; f++) 
				{
					for (int c = 0; c < matriz[0].length; c++) 
					{
						System.out.print(matriz[f][c]+" ");	
					}
					System.out.println("");
				}
				break;
			case 4:
				
				break;
			case 0:
				salir = true;
				break;

			default:
				System.out.println("ERROR!! Para esto creo un menú?! carapene!");
				break;
			}
		}
		sc.close();
	}
}
