package Vectores;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

public class Ejercicio3
{

	public static void main(String[] args)
	{
		//Fer un programa que omplirà un vector de 6 elements de números aleatoris entre 1 i 49, sense repeticions
		//Després demanarà a l’usuari un nº entre 1 i 49 i ens respondrà si aquest número es troba al vector o no. 
		//Finalment, mostrarà el vector
		Random r = new Random();
		Scanner sc = new Scanner(System.in);
		
		HashSet<Integer> vectorSet = new HashSet<Integer>();
		while(vectorSet.size()<6)
		{
			vectorSet.add( r.nextInt(49)+1);
			
//			System.out.println(vectorSet);
//			System.out.println(vectorSet.size());
//			System.out.println(vectorSet.size()<6);
		}
		System.out.println("Pica un numero del 1 al 49");
		int entrada = sc.nextInt();
		
		if (entrada < 50 && entrada > 0)
		{
			if (vectorSet.contains(entrada))
			{
				System.out.println("Tu numero se encuentra en nuestro set ;)\n"+vectorSet);
			}
			else
			{
				System.out.println("Tu numero no se encuentra en nuestro set \n"+vectorSet);

			}
		}
		else
		{
			System.out.println("Pa eso te digo que franja de numeros has de poner? carapene!");
		}
	}

}
