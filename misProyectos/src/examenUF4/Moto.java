package examenUF4;

public class Moto extends Vehiculo
{
	int cilindrada;
	int idM;
	
	public Moto(String matricula, String modelo, int numAños, TipoEstado estado, int cilindrada, int idM)
	{
		super(matricula, modelo, numAños, estado);
		this.cilindrada = cilindrada;
		this.idM = idM;
	}

	@Override
	public void comprobar() 
	{
		if (numAños >= 4)
		{
			estado = TipoEstado.BAJA;
		}
		
	}


}
