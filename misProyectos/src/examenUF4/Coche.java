package examenUF4;

public class Coche extends Vehiculo 
{
	int plazas;
	int idC;
	
	
	
	public Coche()
	{
		
	}
	public Coche(String matricula, String modelo, int numAños, TipoEstado estado, int plazas, int idC)
	{
		super(matricula, modelo, numAños, estado);
		this.plazas = plazas;
		this.idC = idC;
	}


	@Override
	public void comprobar() 
	{
		if (numAños >= 4)
		{
			estado = TipoEstado.BAJA;
		}
		
	}

}
