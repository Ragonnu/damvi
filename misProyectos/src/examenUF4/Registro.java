package examenUF4;

import java.util.ArrayList;

public class Registro 
{
	ArrayList<Vehiculo> datos = new ArrayList<Vehiculo>();
	int max = 6;
	int actual;
	
	
	public Vehiculo obtenerVehiculo(int n)
	{
		return datos.get(n);
	}
	
	public int nuevoVehiculo(Vehiculo vehiculo)
	{
		int retorno = 0;
		actual++;
		
		if (actual > 1)
		{
			for (Vehiculo v : datos)
			{
				if(v.matricula.equals(vehiculo.matricula))
				{
					System.out.println("Este vehiculo ya esta dado de alta");
					actual--;
					retorno = -1;
				}
				else
				{
					if (actual > max)
					{
						System.out.println("No podemos añadir mas vehiculos");
						actual--;
						retorno = -1;
					}
					else
					{
						System.out.println("Vehiculo registrado correctamente");
						datos.add(vehiculo);
						retorno = actual;
					}
				}
			}
		}
		else
		{
			System.out.println("Vehiculo registrado correctamente");
			datos.add(vehiculo);
			retorno = actual;
		}
		return retorno;
	}
}
