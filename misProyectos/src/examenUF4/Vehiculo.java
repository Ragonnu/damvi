package examenUF4;


public abstract class Vehiculo implements Desechable
{
	
	String matricula;
	String modelo;
	int numAños;
	TipoEstado estado;
	
	public Vehiculo ()
	{
		
	}
	public Vehiculo(String matricula, String modelo, int numAños, TipoEstado estado)
	{
		this.matricula = matricula;
		this.modelo = modelo;
		this.numAños = numAños;
		this.estado = estado;
	}

	@Override
	public String toString()
	{
		return "Vehiculo [matricula=" + matricula + ", modelo=" + modelo + ", numAños=" + numAños + ", estado=" + estado
				+ "]";
	}
	
	public boolean disponible(Vehiculo vehiculo)
	{
		boolean disponible;
		
		if (vehiculo.estado.equals(TipoEstado.BAJA)||vehiculo.estado.equals(TipoEstado.ALQUILADO)||vehiculo.estado.equals(TipoEstado.AVERIADO))
		{
			disponible = false;
		}
		else
		{
			disponible = true;
		}
		return disponible;
	}
	
	public void alquilar (Vehiculo vehiculo)
	{
		if (disponible(vehiculo))
		{
			vehiculo.estado = TipoEstado.ALQUILADO;
			System.out.println("Este vehiculo ha sido "+vehiculo.estado);
		}
		else
		{
			System.out.println("Este vehiculo no se puede alquilar porque su estado es: "+vehiculo.estado);
		}
	}
	public void retornar (Vehiculo vehiculo)
	{
		if (vehiculo.estado.equals(TipoEstado.ALQUILADO))
		{
			vehiculo.estado = TipoEstado.DISPONIBLE;
			System.out.println("Este vehiculo ha sido "+vehiculo.estado);
		}
		else
		{
			System.out.println("Este vehiculo no se puede retornar porque su estado es: "+vehiculo.estado);
		}
	}
	
	public void reparar(Vehiculo vehiculo)
	{
		if (vehiculo instanceof Coche)
		{
			if (vehiculo.numAños >= 3)
			{
				vehiculo.estado = TipoEstado.BAJA;
				System.out.println("Este vehiculo es muy viejo para ser reparado");
			}
			else
			{
				if (vehiculo.estado.equals(TipoEstado.ALQUILADO)||vehiculo.estado.equals(TipoEstado.BAJA))
				{
					System.out.println("No se puede reparar, estado: "+vehiculo.estado);
				}
				else
				{
					vehiculo.estado = TipoEstado.AVERIADO;
					System.out.println("Ha reparar!");
				}
			}
		}
		else if (vehiculo instanceof Moto)
		{
			if (vehiculo.numAños >= 2)
			{
				vehiculo.estado = TipoEstado.BAJA;
				System.out.println("Este vehiculo es muy viejo para ser reparado");
			}
			else
			{
				if (vehiculo.estado.equals(TipoEstado.ALQUILADO)||vehiculo.estado.equals(TipoEstado.BAJA))
				{
					System.out.println("No se puede reparar, estado: "+vehiculo.estado);
				}
				else
				{
					vehiculo.estado = TipoEstado.AVERIADO;
					System.out.println("Ha reparar!");
				}
			}
		}
		else if (vehiculo instanceof Bicicleta)
		{
			if (vehiculo.estado.equals(TipoEstado.ALQUILADO)||vehiculo.estado.equals(TipoEstado.BAJA))
			{
				System.out.println("No se puede reparar, estado: "+vehiculo.estado);
			}
			else
			{
				vehiculo.estado = TipoEstado.AVERIADO;
				System.out.println("Ha reparar!");
			}
		}
	}
	
	public void visualizar()
	{
		System.out.println(toString());
	}
	
	public void clonar(Vehiculo vehiculo)
	{
		this.matricula = vehiculo.matricula;
		this.modelo = vehiculo.modelo;
		this.numAños = vehiculo.numAños;
		this.estado = vehiculo.estado;
	}
	
	
	
	
	
}
