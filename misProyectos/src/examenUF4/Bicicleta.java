package examenUF4;

public class Bicicleta extends Vehiculo
{

	int idB;
	
	public Bicicleta(String matricula, String modelo, int numAños, TipoEstado estado, int idB)
	{
		super(matricula, modelo, numAños, estado);
		this.idB = idB;
	}

	@Override
	public void comprobar() 
	{
		if (numAños >= 4)
		{
			estado = TipoEstado.BAJA;
		}
		
	}
}
