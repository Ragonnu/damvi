package examen;

import java.util.Scanner;

public class Ejercicio4Nombre
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		String nombre = sc.nextLine();
		char letra = nombre.charAt(0);
		boolean flag = false;
		
		for (int i = 0; i < nombre.length(); i++)
		{
			
			if(nombre.charAt(i)!=letra) 
			{
				flag = true;
			}
			letra = nombre.charAt(i);
		}
		if (!flag)
		{
			System.out.println("Este nombre contiene solo letras iguales");
		}
		else
		{
			System.out.println("Nombre valido");
		}
	}
	

}
