package examen;

public class Ejercicio1Veri 
{

	public static void main(String[] args)
	{
		int vidaMonstruo = 100;
		int atcJungla = 10;
		int veneno = 20 ;
		int acc = 0;
		
		while (vidaMonstruo > 0)
		{
			acc++;
			
			vidaMonstruo = vidaMonstruo - atcJungla;
			if (vidaMonstruo <= 0)
			{
				System.out.println("El monstruo murio a manos del Junglero en "+acc+" turnos");
				break;
			}
			vidaMonstruo = vidaMonstruo - veneno;
			if (vidaMonstruo <= 0)
			{
				System.out.println("El monstruo murio a manos Twich en "+acc+" turnos");
			}
		}
	}

}
