package examen;

import java.util.Scanner;

public class menu 
{

	public static void main(String[] args) 
	{
		System.out.println("Menú");
		System.out.println("1-Subir volumen");
		System.out.println("2-Bajar volumen");
		System.out.println("3-Mostrar volumen");
		System.out.println("4-Salir");
		Scanner sc = new Scanner(System.in);
		
		int volumen = 1;
		boolean salir = false;
		
		while (!salir)
		{
			
			
			int n = sc.nextInt();
			
			switch (n) 
			{
			case 1:
				if (volumen < 5) 
				{
					volumen++;
				} 
				else
				{
					System.out.println("Volumen al máximo");
				}
				break;
			case 2:
				if (volumen > 1) 
				{
					volumen--;
				} 
				else 
				{
					System.out.println("Volumen al mínimo");
				}
				break;
			case 3:
				for (int i = 1; i < 6; i++)
				{
					if (i == volumen)
					{
						System.out.println(i + "X");
					} 
					else 
					{
						System.out.println(i);
					}
				}
				break;
			case 4:
				salir = true;
				break;

			default:
				System.out.println("ERROR!! Para esto creo un menú?! carapene!");
				break;
			}
		}
		sc.close();
	}

}
