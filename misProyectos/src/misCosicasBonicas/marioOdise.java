package misCosicasBonicas;

import java.util.Scanner;

public class marioOdise 
{
//
	public static void main(String[] args) 
	{

		int dLanzamineto = 27;
		int hLanzamineto = 10;
		
		int dRestantes = 0;
		int hRestantes = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Que dia de mes es hoy (1-31)?");
		int d = sc.nextInt();
		
		System.out.println("Que hora es (0-23)?");
		int h = sc.nextInt();
		
		if (d > 31 || h > 23)
		{
			System.out.println("ERROR! Has puesto mal el dia o/y la hora, carapene!!");
		}
		else
		{	
			if (d > dLanzamineto)
			{
				System.out.println("El juego ya esta a la venta! corre a comprarlo!");
			}
			else if (d == dLanzamineto)
			{
				if (h >= hLanzamineto)
				{
					System.out.println("El juego ya esta a la venta! corre a comprarlo!");
				}
				else
				{
					hRestantes = hLanzamineto - h;
					System.out.println("Faltan "+hRestantes+" horas para que salga el videojuego");
				}
			}
			else
			{
				hRestantes = hLanzamineto - h;
				dRestantes = dLanzamineto - d;
				if (hRestantes < 0)
				{
					dRestantes--;
					hRestantes = 24 + hRestantes;
				}
				System.out.println("Faltan "+ dRestantes +" dias y "+hRestantes+" horas para el lanzamiento del juego.");
			}
			
			sc.close();
		}
	}
}
