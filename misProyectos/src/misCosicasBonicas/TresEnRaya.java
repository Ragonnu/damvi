package misCosicasBonicas;

import java.util.Scanner;

public class TresEnRaya 
{

	// variable global: se usa en mas de una funcion, se declara fuera y estatica
	static char[][] tablero = new char[3][3];
	static boolean turnob;
	static int turnoi;
	static char turnoc;
	static String turnos;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) 
	{

		// siempre vamos a tener una funcion de inicializacion
		init();
		// un turno
		int arcaico=3;
		while (arcaico == 3) 
		{
			turnoc = cambiarTurnoc(turnoc);
			ponerFicha(turnoc);
			//arcaico = comprobarVictoria();
			//turnob = cambiarTurnob(turnob);
			//turnoi = cambiarTurnoi(turnoi);
			
			//turnos = cambiarTurnos(turnos);
			show();
		
					
		}

	}

	private static void show() 
	{
		for (int f = 0; f < tablero.length; f++) 
		{
			for (int c = 0; c < tablero[0].length; c++) 
			{
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}		
	}

	private static void init() 
	{
		for (int f = 0; f < tablero.length; f++) 
		{
			for (int c = 0; c < tablero[0].length; c++) 
			{
				tablero[f][c]='.';
			}
		}
	}

	private static void ponerFicha(char turnoc2)
	{
		boolean repetir = true;
		while (repetir)
		{
			System.out.println("Escribe dos numeros, el primero para la fila y el segundo para la columna.");
			int fil = sc.nextInt();
			int col = sc.nextInt();
			if (tablero[fil][col] == '.')
			{
				tablero[fil][col] = turnoc2;
				repetir=false;
			} 
			else 
			{
				System.out.println("ERROR! casilla ocupada. ");
			} 
		}
		
	}

	private static String cambiarTurnos(String turnos2) 
	{
		if(turnos2.equals("卐")) 
		{
			return "✡";
		}else {
			return "卐";
		}
	}

	private static char cambiarTurnoc(char turnoc2) 
	{
		if(turnoc2=='X') 
		{
			return 'O';
		}else 
		{
			return 'X';
		}
	}

	private static int cambiarTurnoi(int turnoi2) 
	{
		return (turnoi2+1)%2;
	}

	private static boolean cambiarTurnob(boolean turnob2) 
	{
		return !turnob;
	}

	private static int comprobarVictoria() 
	{
		//TODO
		return 0;
	}

}
