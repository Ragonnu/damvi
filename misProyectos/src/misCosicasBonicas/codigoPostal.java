package misCosicasBonicas;

import java.util.Scanner;

public class codigoPostal 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Dame un codigo postal de Sabadell");
		
		String cp = sc.nextLine();
		String pueblo = "";
		
		switch (cp) 
		{
		case "08201":
			pueblo = "Centro";
			break;
		case "08202":
			pueblo = "Torre Romeo";
			break;
		case "08203":
			pueblo = "Eixample";
			break;
		case "08204":
			pueblo = "Cruz de Barbara";
			break;
		case "08205":
			pueblo = "Gracia";
			break;
		case "08206":
			pueblo = "Can Rul";
			break;
		case "08207":
			pueblo = "Can'Oriac";
			break;
		case "08208":
			pueblo = "Creu Alta";
			break;
		default:
			System.out.println("Error!!Este codigo postal no pertenece a sabadell.");
			break;
		}
		System.out.println(pueblo);
		sc.close();
	}

}
