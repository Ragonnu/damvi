package misCosicasBonicas;

import java.util.Scanner;

public class cambioMinimo
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Cuantos centimos tienes?");
		int nCentimos = sc.nextInt();
		
		System.out.println("Cuantos euros tienes?");
		int nEuros = sc.nextInt();
		
		nCentimos = nCentimos + (nEuros * 100);
		System.out.println(nCentimos+" centimos");
		
		int c1 = 0;
		int c2 = 0;
		int c5 = 0;
		int c10 = 0;
		int c20 = 0;
		int c50 = 0;
		int e1 = 0;
		int e2 = 0;
		int b5 = 0;
		int b10 = 0;
		int b20 = 0;
		int b50 = 0;
		int b100 = 0;
		int b200 = 0;
		int b500 = 0;
		
		
		if (nCentimos >= 50000)
		{
			b500 = nCentimos / 50000;
			nCentimos = nCentimos % 50000;
			
		}
		if (nCentimos >= 20000)
		{
			b200 = nCentimos / 20000;
			nCentimos = nCentimos % 20000;
		}
		if (nCentimos >= 10000)
		{
			b100 = nCentimos / 10000;
			nCentimos = nCentimos % 10000;
		}
		if (nCentimos >= 5000)
		{
			b50 = nCentimos / 5000;
			nCentimos = nCentimos % 5000;
		}
		if (nCentimos >= 2000)
		{
			b20 = nCentimos / 2000;
			nCentimos = nCentimos % 2000;
		}
		if (nCentimos >= 1000)
		{
			b10 = nCentimos / 1000;
			nCentimos = nCentimos % 1000;
		}
		if (nCentimos >= 500)
		{
			b5 = nCentimos / 500;
			nCentimos = nCentimos % 500;
		}
		if (nCentimos >= 200)
		{
			e2 = nCentimos / 200;
			nCentimos = nCentimos % 200;
		}
		if (nCentimos >= 100)
		{
			e1 = nCentimos / 100;
			nCentimos = nCentimos % 100;
		}
		if (nCentimos >= 50)
		{
			c50 = nCentimos / 50;
			nCentimos = nCentimos % 50;
		}
		if (nCentimos >= 20)
		{
			c20 = nCentimos / 20;
			nCentimos = nCentimos % 20;
		}
		if (nCentimos >= 10)
		{
			c10 = nCentimos / 10;
			nCentimos = nCentimos % 10;
		}
		if (nCentimos >= 5)
		{
			c5 = nCentimos / 5;
			nCentimos = nCentimos % 5;
		}
		if (nCentimos >= 2)
		{
			c2 = nCentimos / 2;
			nCentimos = nCentimos % 2;
		}
		c1 = nCentimos;
		
		System.out.println(b500+" Billetes de 500 euros");
		System.out.println(b200+" Billetes de 200 euros");
		System.out.println(b100+" Billetes de 100 euros");
		System.out.println(b50+" Billetes de 50 euros");
		System.out.println(b20+" Billetes de 20 euros");
		System.out.println(b10+" Billetes de 10 euros");
		System.out.println(b5+" Billetes de 5 euros");
		System.out.println(e2+" monedas de 2 euros");
		System.out.println(e1+" monedas de 1 euro");
		System.out.println(c50+" monedas de 50 centimos");
		System.out.println(c20+" monedas de 20 centimos");
		System.out.println(c10+" monedas de 10 centimos");
		System.out.println(c5+" monedas de 5 centimos");
		System.out.println(c2+" monedas de 2 centimos");
		System.out.println(c1+" monedas de 1 centimos");
		sc.close();
	}
}
