package misCosicasBonicas;

import java.util.Scanner;

public class marioOside2 
{

	public static void main(String[] args) 
	{
		//Super Mario Odyssey
		
		System.out.println("Dime una letra en minuscula:\n");
		Scanner sc = new Scanner(System.in);
		String letra = sc.nextLine();
		
		switch (letra)
		{
		case "u":
		case "p":
			System.out.println("Esta letra pertenece a la primera palabra: SUPER");
			break;

		case "m":
		case "a":
		case "i":
			System.out.println("Esta letra pertenece a la segunda palabra: MARIO");
			break;
			
		case "d":
		case "y":
			System.out.println("Esta letra pertenece a la tercera palabra: ODYSSEY");
			break;
			
		case "r":
			System.out.println("Esta letra pertenece a la primera y la segunda palabra: SUPER y MARIO");
			break;
			
		case "s":
		case "e":
			System.out.println("Esta letra pertenece a la primera y la tercera palabra: SUPER y ODYSSEY");
			break;
			
		case "o":
			System.out.println("Esta letra pertenece a la segunda y la tercera palabra: MARIO y ODYSSEY");
			break;
			
		default:
			System.out.println("ERROR! Letra invalida!");
			break;
		}
		sc.close();
	}

}
