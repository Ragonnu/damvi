package misCosicasBonicas;

import java.util.Scanner;

public class a4Exercici12 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		double numEntrada = 0;
		
		double numGran = 0;
		double numPetit = 1000000000;
		double mitjana = 0;
		double suma = 0;
		
		for (int i = 1; i <= 10; i++)
		{
			 numEntrada = sc.nextDouble();
			 
			 if (numEntrada > numGran) 
			 {
				numGran = numEntrada;
			 }
			 if (numEntrada < numPetit)
			 {
				numPetit = numEntrada;
			 }
			 
			 suma = suma + numEntrada;
			 mitjana = suma / i;
			 
		}
		int numGrande = (int)numGran;
		int numPequeño = (int)numPetit;
		System.out.println("El numero mas grande ha sido: "+numGrande+", el numero mas pequeño fue: "+numPequeño+" y la media es: "+mitjana);
		sc.close();
	}

}
