package misCosicasBonicas;

import java.util.Scanner;

public class cosasSobreStrings 
{

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int a = 4;
		
		String st = "patata";
		////Metodes sobre les strings
		
		
		///utils
		String mayus = st.toUpperCase();
		System.out.println(mayus);
		System.out.println(mayus.toLowerCase());
		
		
		///recorrer String
		
		System.out.println(st.length());
		System.out.println(st.charAt(0));
		
		//contar A
		int longitud = st.length();
		int contadorParaContar = 0;
		for(int i = 0; i < longitud; i++) {
			if(st.charAt(i)=='a') {
				contadorParaContar++;
			}
		}
		System.out.println(contadorParaContar);
		
		
		
		///substring
		String subs = st.substring(3, 6);
		System.out.println(subs);
		
		//Split
		String frase = "hola me gustan las patatas y los gatetes y git no";
		String[] array = frase.split(" ");
		System.out.println(array.length);
		
		
		///conversion
		
		String coord = "G8";
		
		int fila;
		int columna;
		
		char primeraCoord = coord.charAt(0);
		String segundaCoord = coord.substring(1, 2);
		
		System.out.println(segundaCoord);
		//la primera se haria con un switch
		
		int segcoord = Integer.parseInt(segundaCoord);
		
		segcoord++;
		
		System.out.println(segcoord);
		
	//https://gitlab.com/DAM-M03/m03-uf1.git
	}

}
