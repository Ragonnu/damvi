package misCosicasBonicas;

import java.util.Scanner;

public class autocorrector 
{

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Dime la nota del ejercicio 1: ");
		double e1 = sc.nextDouble();
		
		System.out.println("Dime la nota del ejercicio 2: ");
		double e2 = sc.nextDouble();
		
		System.out.println("Dime la nota del examen: ");
		double examen = sc.nextDouble();
		Double ponderada = 0.0;

		if (e1 == 0 || e2 == 0 || examen <= 4)
		{
			System.out.println("Suspendido!");
		}
		else
		{
			ponderada =  (e1*0.1)+(e2*0.3)+(examen*0.6);
			if (ponderada >= 5.0)
			{
				System.out.println("Aprovado!");
			}
			else
			{
				System.out.println("Suspendido!");
			}
		}
		sc.close();
	}

}
