package buscaminas;

import java.util.Random;
import java.util.Scanner;

public class PBuscaminas
{
	
	static int min = 3;
	static int max = 12;
	static boolean flag = true;
	static String nombre = " ";
	static Scanner sc = new Scanner(System.in);
	static int fila = 7;
	static int columna = 7;
	static int numeroMinas = 41;
	static int [][]tablero = new int [fila][columna];
	static int[][] tableroMinas = new int[fila][columna];
	static Random r = new Random();
	static int count = 0;
	static int casilla;
	
	public static void main(String[] args) 
	{
	
//		int max = 12;

//		int vacio = 0;
//		int mina = 1;
//		int tapado = 9;
		
		
		
		
		do 
		{
			System.out.println(" "+" "+" "+"BUSCAMINAS");
			System.out.println("");
			System.out.println("1.- Ayuda");
			System.out.println("2.- Opciones");
			System.out.println("3.- Jugar");
			System.out.println("4.- Ver Rankings");
			System.out.println("0.- Salir");
			System.out.println("");
			System.out.println("Marca una opción");

			int opcion = sc.nextInt();
			sc.nextLine();
			switch (opcion)
			{
			case 1:
				ayuda();
				break;
			case 2:
				opciones();
				break;
			case 3:
				init();
				mostrar();
				jugar();
				break;
			case 4:
				rankings();
				break;
			case 0:
				salir();
				break;

			default:
				System.out.println("ERROR! Marca un numero del 0 al 4");
				break;
			}
		} 
		while (flag);
	}

	private static void mostrar() 
	{
		// TODO Auto-generated method stub
		for (int fila = 0; fila < tableroMinas.length; fila++)
		{
			for (int columna = 0; columna < tableroMinas[0].length; columna++)
			{
				System.out.print(tableroMinas[fila][columna]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
		for (int fila = 0; fila < tablero.length; fila++)
		{
			for (int columna = 0; columna < tablero[0].length; columna++)
			{
				System.out.print(tablero[fila][columna]+" ");
			}System.out.println();
		}
	}

	private static void init() 
	{
		tablero = new int [fila][columna];
		for (int fila = 0; fila < tablero.length; fila++) 
		{
			for (int columna = 0; columna < tablero[0].length; columna++) 
			{
				tablero[fila][columna]= 9;
			}
		
		}
		
		tableroMinas = new int[fila][columna];
		while (count != numeroMinas) 
		{
			for (int fila = 0; fila < tableroMinas.length; fila++) 
			{
				for (int columna = 0; columna < tableroMinas[0].length; columna++) 
				{
					if (tableroMinas[fila][columna] == 0)
					{
						if (count == numeroMinas)
						{
							tableroMinas[fila][columna] = 0;
						} else
						{
							casilla = r.nextInt(2);
							tableroMinas[fila][columna] = casilla;
							if (casilla == 1)
							{
								count++;
							}
						} 
					}
					else
					{
						tableroMinas[fila][columna] = 1;
					}
				}

			} 
		}
		System.out.println(count);
	}

	private static void salir() 
	{
		System.out.println("Gracias por jugar, adios");
		flag = false;
	}

	private static void rankings() 
	{
		// TODO Auto-generated method stub
		System.out.println(nombre+" "+fila+" "+"Estas en rankings"+" "+columna);
	}

	private static void jugar() 
	{
		// TODO Auto-generated method stub
	
		System.out.println("Estas jugando");
	}

	private static void opciones()
	{
		System.out.println("Que nombre quieres usar?");
		nombre = sc.nextLine();
		System.out.println("cuantas filas quieres que tenga el mapa?");
		fila = sc.nextInt() ;
		System.out.println("cuantas columnas quieres que tenga el mapa?");
		columna = sc.nextInt();
		System.out.println("cuantas minas quieres en el mapa?");
		numeroMinas = sc.nextInt();
		
	}

	private static void ayuda() 
	{
		System.out.println("El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.\n"
				+ "Algunas casillas tienen un número, el cual indica la cantidad de minas que hay en las casillas circundantes.\n"
				+ "Así, si una casilla tiene el número 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o borde) \n"
				+ "hay 3 con minas y 5 sin minas. Si se descubre una casilla sin número indica que ninguna de las casillas vecinas tiene mina y éstas \n"
				+ "se descubren automáticamente.\r\n"
				+ "Si se descubre una casilla con una mina se pierde la partida.");
		System.out.println();
	
	}

}
