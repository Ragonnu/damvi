package MatricesChulas;

import java.util.Random;
import java.util.Scanner;

///Joc amb un taulell 2D
public class Orto 
{

	static String[] sprites = { "", "char.png", "calabaza.png", "forat.png", "seta3.png", "rip.png", "", "nz.png" };
	static boolean sortir = false;
	static int acc;
	static int naziy;
	static int nazix;
	static int x;
	static int y;
	static int[][] tablero;
	static boolean flagNazi=false;
	public static void main(String[] args) 
	{

		Scanner sc = new Scanner(System.in);
		tablero = new int[7][7];

		Board b = new Board();
		Window w = new Window(b);

		b.setActcolors(false);
		// activar modo sprites
		b.setActsprites(true);
		b.setActimgbackground(true);
		// creo el vector de sprites a mano
		//String[] sprites = { "", "char.png", "calabaza.png", "hole.png", "", "", "", "nz.png" };
		//String[] sprites = { "", "seta2.png", "seta.jpg", "hole.png", "", "", "", "nz.png" };

		// le paso el vector que acabo de crear al motor
		b.setSprites(sprites);
		// le paso al motor un fondo
		b.setImgbackground("ger.png");
		//b.setImgbackground("blanco.png");

		Utils.view(tablero);
		b.draw(tablero);

		Random r = new Random();
		/// personaje sera un 1.
		nazix = r.nextInt(7);
		naziy = r.nextInt(7);

		int calabazas = 0;
		x = 3;
		y = 3;
		tablero[x][y] = 1;
		tablero[nazix][naziy] = 7;
		tablero[r.nextInt(7)][r.nextInt(7)] = 2;
		tablero[r.nextInt(5)+1][r.nextInt(5)+1] = 3;

		Utils.view(tablero);
		b.draw(tablero);
	

		
		while (!sortir)
		{
			char opt = sc.nextLine().charAt(0);
			tablero[nazix][naziy] = 3;
			switch (opt)
			{
			case 'w':
				if (x == 0) 
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					x--; 
					if (tablero[x][y] == 2) 
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					if (tablero[x][y] == 3)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else 
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 'a':
				if (y == 0)
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y--;
					if (tablero[x][y] == 2) 
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					if (tablero[x][y] == 3)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 's':
				if (x == tablero.length - 1) 
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					x++;
					if (tablero[x][y] == 2) 
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					//					
					if (tablero[x][y] == 3) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else 
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 'd':
				if (y == tablero[0].length - 1) 
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y++;
					if (tablero[x][y] == 2)
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					if (tablero[x][y] == 3) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else 
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 'q':
				if (y == 0||y == 1)
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y = y - 2;
					if (tablero[x][y] == 2) 
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					if (tablero[x][y] == 3)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 'e':
				if (y == tablero[0].length - 1||y == tablero[0].length - 2) 
				{
					System.out.println("te vas a caer por el borde de la tierra plana");
				} 
				else 
				{
					// borrar pos actual
					tablero[x][y] = 0;
					// actuatlizar nueva pos
					y = y + 2;
					if (tablero[x][y] == 2)
					{
						System.out.println("has salvado a tu gente");
						calabazas++;
						tablero[r.nextInt(7)][r.nextInt(7)] = 2;
					}
					if (tablero[x][y] == 7)
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					if (tablero[x][y] == 3) 
					{
						System.out.println("pa'l lobby");
						tablero[x][y] = 5;
						sortir = true;
						break;
					}
					else 
					{
						tablero[x][y] = 1;
					}
				}
				break;
			case 'p':
				System.out.println("has salvado " + calabazas + " veces a tu gente");
				sortir = true;
			default:
				break;
			}
			if (!flagNazi) 
			{
				movimienton();
			}
			Utils.view(tablero);
			b.draw(tablero);
			acc++;
//			if (acc >= 3)
//			{
//				flagNazi = false;
//				acc = 0;
//			}
		}

	}

	private static void movimienton() 
	{
		
			// TODO movimiento ciudadano
			int distx = x - nazix;
			int disty = y - naziy;
			tablero[nazix][naziy] = 0;
			if (Math.abs(distx) > Math.abs(disty))
			{
				if (distx > 0)
				{
					nazix++;
				} 
				else
				{
					nazix--;
				}
			} 
			else 
			{
				if (disty > 0) 
				{
					naziy++;
				}
				else
				{
					naziy--;
				}
			}
			if (tablero[nazix][naziy] == 1) {
				System.out.println("tremendo cogid�n en la funcion movimient�n");
				tablero[x][y] = 5;
				sortir = true;
			}
			
			if (tablero[nazix][naziy] == 3) 
			{
				flagNazi = true;
				tablero[nazix][naziy] = 4;
				
			}
			else
			{
				tablero[nazix][naziy] = 7;
			}
			

	}

}
