package MatricesChulas;

import java.util.Random;
import java.util.Scanner;

public class Utils {
	
	public static void view(int[][] tablero) {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}
	
	
	public void view2(int[][] tablero) {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}
	
	
	public static int[][] rellenarAMano(int fil, int col){
		int[][] tablero = new int[fil][col];
		Scanner sc = new Scanner(System.in);
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.println("escribe numero para la fila "+f+" columna "+c);
				tablero[f][c]=sc.nextInt();
			}
		}
		return tablero;
		
	}
	
	
	public static int[][] rellenarRandom(int fil, int col, int rangeInf, int rangeSup){
		int[][] tablero = new int[fil][col];
		Random rRrRr = new Random();
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[f][c] = rRrRr.nextInt(rangeSup-rangeInf)+rangeInf;
			}
		}
		return tablero;
		
	}
	
	public static int[] sumarFiles(int[][] tablero) {
		int acc = 0;
		int[] sumaDeFiles = new int[tablero.length];
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				acc+= tablero[f][c];
			}
			sumaDeFiles[f]=acc;
			acc=0;
		}
		return sumaDeFiles;
	}
	
	
	public static int[] sumarColumnes(int[][] tablero) {
		int acc = 0;
		int[] sumaDeColumnes = new int[tablero[0].length];
		///trabajad OompaLoompas
		return sumaDeColumnes;
	}
	

}
