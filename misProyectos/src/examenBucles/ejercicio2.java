package examenBucles;

import java.util.Scanner;

public class ejercicio2 
{
//Fes un menú amb 4 opcions
//	Opció 1: Fer una operació aritmètica
//	Opció 2: Fer l’exercici 1
//	Opció 3: Saludar
//	Opció 4: Sortir
//	El menú t’ha d’insultar durament en cas d’opció incorrecta

	public static void main(String[] args) 
	{
		System.out.println("MENU");
		System.out.println("1: Operación");
		System.out.println("2: Murciegalo");
		System.out.println("3: Saludar");
		System.out.println("4: salir");
		
		Scanner sc = new Scanner(System.in);
		int entrada = sc.nextInt();
		int a = 5;
		int b = 2;
		switch (entrada)
		{
		case 1:
			System.out.println("operación aritmética: 5*2="+(a*b));
			break;
		case 2:
			System.out.println("Escribe una frase");
			Scanner sc2 = new Scanner(System.in);
			String frase = sc2.nextLine();
			
			int longitud = frase.length();
			boolean av = false;
			boolean ev = false;
			boolean iv = false;
			boolean ov = false;
			boolean uv = false;
			
			for(int j = 0; j < longitud; j++)
			{
				switch (frase.charAt(j)) 
				{
				case 'a':
					av = true;
					break;
				case 'e':
					ev = true;
					break;
				case 'i':
					iv = true;
					break;
				case 'o':
					ov = true;
					break;
				case 'u':
					uv = true;
					break;
					
				}
			}
			if (av && ev && iv && ov && uv)
			{
				System.out.println("TOTES");
			}
			else 
			{
				System.out.print("FALTEN");
			}
			if (av != true) 
			{
				System.out.print(" a");
			}
			if (ev != true) 
			{
				System.out.print(" e");
			}
			if (iv != true) 
			{
				System.out.print(" i");
			}
			if (ov != true) 
			{
				System.out.print(" o");
			}
			if (uv != true) 
			{
				System.out.print(" u");
			}
			break;
		case 3:
			System.out.println("Hola, que tal?");
			break;
		case 4:
			break;
		default:
			System.out.println("Error! no te has fijado en el menú? Bocachocho!");
			break;
		}
		sc.close();
	}

}
