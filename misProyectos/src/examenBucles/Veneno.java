package examenBucles;

import java.util.Scanner;

public class Veneno 
{

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("vida jugador 1");
		int vida1 = sc.nextInt();
		System.out.println("vida jugador 2");
		int vida2 = sc.nextInt();
		System.out.println("veneno jugador 1");
		int vene1 = sc.nextInt();
		System.out.println("veneno jugador 2");
		int vene2 = sc.nextInt();
		
		while (vida1 > 0 && vida2 > 0) 
		{
			vida1 = vida1 - vene1;
			vida2 = vida2 - vene2;
			
			System.out.println(vida1+" "+vida2);
		}
		if (vida1 <= 0)
		{
			if (vida2 <= 0)
			{
				System.out.println("ambos jugadores han muerto");
			}
			else
			{
				System.out.println("jugador 1 a muerto");
			}
		}
		else 
		{
			System.out.println("jugador 2 a muerto");
		}
		sc.close();
		
	}

}
