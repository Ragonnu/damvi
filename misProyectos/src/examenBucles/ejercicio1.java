package examenBucles;

import java.util.Scanner;

public class ejercicio1 
{
//Escriure un programa que demani com a dades d’entrada a l’usuari una frase que es llegirà de cop.
//El programa escriurà “TOTES” si durant la frase han aparegut totes les vocals, i “FALTEN” si falta alguna vocal, 
//i a més, quines vocals falten

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		String frase = sc.nextLine();
		
		int longitud = frase.length();
		boolean av = false;
		boolean ev = false;
		boolean iv = false;
		boolean ov = false;
		boolean uv = false;
		
		for(int j = 0; j < longitud; j++)
		{
			switch (frase.charAt(j)) 
			{
			case 'a':
				av = true;
				break;
			case 'e':
				ev = true;
				break;
			case 'i':
				iv = true;
				break;
			case 'o':
				ov = true;
				break;
			case 'u':
				uv = true;
				break;
				
			}
		}
		if (av && ev && iv && ov && uv)
		{
			System.out.println("TOTES");
		}
		else 
		{
			System.out.print("FALTEN");
		}
		if (av != true) 
		{
			System.out.print(" a");
		}
		if (ev != true) 
		{
			System.out.print(" e");
		}
		if (iv != true) 
		{
			System.out.print(" i");
		}
		if (ov != true) 
		{
			System.out.print(" o");
		}
		if (uv != true) 
		{
			System.out.print(" u");
		}
		sc.close();

	}

}
