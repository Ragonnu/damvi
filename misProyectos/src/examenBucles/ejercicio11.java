package examenBucles;

import java.util.Scanner;

public class ejercicio11 
{
//Algorisme que llegeix un número enter N i ens diu la llista de números que són divisors d’aquest.
	
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		for (int i = 1; i <= n; i++)
		{
			if (n % i == 0)
			{
				System.out.println(i);
			}
		}
		sc.close();
	}

}
