package examenBucles;

import java.util.Scanner;

public class Discord2 
{

	public static void main(String[] args)
	{
//		simplemente tienes que recorrer una String
//		y decir si tiene dos letras iguales contiguas o no
//		por ejemplo
//		patata - NO
//		paatata - SI
//		poma - NO
//		pressec - SI
		
		Scanner sc = new Scanner(System.in);
		String palabra = sc.nextLine();
		boolean flag = false;
		char letra = ' ';
		
		for (int i = 0; i < palabra.length(); i++)
		{
			if (palabra.charAt(i) == letra)
			{
				flag = true;
			} 
			
			letra = palabra.charAt(i);
			//System.out.println(letra);
		}
		String prueba = palabra.substring(1, 6);
		System.out.println(prueba);
		System.out.println(palabra+" "+flag);
		sc.close();
	}

}
