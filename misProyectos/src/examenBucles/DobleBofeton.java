package examenBucles;

import java.util.Random;

public class DobleBofeton 
{

	public static void main(String[] args)
	{
		Random r = new Random();
		boolean caraoCreu = true;
		int acc = 2;

		while (caraoCreu && acc < 5) 
		{
			caraoCreu = r.nextBoolean();
			
			if (caraoCreu)
			{
				 acc++;
			}
		}
		System.out.println(acc);
		
	}

}
