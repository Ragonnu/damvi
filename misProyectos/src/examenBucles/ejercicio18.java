package examenBucles;

import java.util.Scanner;

public class ejercicio18 
{
//Algorisme que llegeix una seqüència de lletres (que formen una frase) fins arribar al punt.
//La sortida ens diu quantes paraules conté la frase introduïda.
	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		String frase = sc.nextLine();
		int acc = 0;
		
		for(int i=0 ; i < frase.length(); i++) 
		{
			if(frase.charAt(i) == ' ') 
			{
				acc++;
			}
		}
		
		System.out.println(acc+1+" palabras");
		sc.close();
	}

}
