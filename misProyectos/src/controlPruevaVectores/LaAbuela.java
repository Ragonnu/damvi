package controlPruevaVectores;

import java.util.ArrayList;
import java.util.Scanner;

public class LaAbuela 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		ArrayList<Integer> lista1 = new ArrayList<Integer>();
		System.out.println("Mete 10 numeros para la primera fila de dientes");
		
		while(lista1.size()<10)
		{		

			lista1.add( sc.nextInt());
		}
		
		ArrayList<Integer> lista2 = new ArrayList<Integer>();
		System.out.println("Mete 10 numeros para la segunda fila de dientes");
		
		while(lista2.size()<10)
		{		

			lista2.add( sc.nextInt());
		}
		
		int prueba1 = (lista1.get(0))+(lista2.get(0));
		int prueba2 = 0;
		boolean flag = true;
		for (int i = 1; i < lista1.size(); i++) 
		{
			prueba2 = (lista1.get(i))+(lista2.get(i));
			if (prueba1!=prueba2)
			{
				flag = false;
			}
			prueba1 = prueba2;
		}
		if (flag)
		{
			System.out.println("SI");
		}
		else
		{
			System.out.println("NO");
		}
	}

}
