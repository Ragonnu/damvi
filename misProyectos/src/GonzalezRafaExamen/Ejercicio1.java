package GonzalezRafaExamen;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Ejercicio1 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
//		System.out.println("Introduce una palabra");
		
		String palabraAleatoria = "bonico";
		
		char [] letras = palabraAleatoria.toCharArray();
		char [] guiones = new char[letras.length];		
		for (int i = 0; i < letras.length; i++) 
		{
			guiones[i] = '_';
		}
		
		boolean winer = false;
		boolean flag = false;
		int contadorFallos = 0;

		
		while (winer==false && contadorFallos < 7)
		{
			System.out.println("Introduce una letra");
			
			char letraIntrodicida = sc.next().charAt(0);
			flag=false;
			for (int i = 0; i < letras.length; i++) 
			{
				
				if (letras[i] == letraIntrodicida)
				{
					guiones[i] = letraIntrodicida;
					flag = true;
				}
				
			} 
		
			if (flag==false)
			{
				contadorFallos++;
			}
			System.out.println(guiones);
			System.out.println("Numero de intentos: "+contadorFallos);
			if (Arrays.equals(letras, guiones))
			{
				winer=true;
			}
		}
		System.out.println(guiones);
		
	}

}
