package GonzalezRafaExamen;

import java.util.Scanner;
import java.util.TreeSet;

public class Ejercicio2 
{

	public static void main(String[] args) 
	{
		
		
		boolean flag = false;
		TreeSet<String> lista = new TreeSet<String>();
		
		while (flag==false)
		{
			System.out.println("MENÚ Lista Compra\n1.-Introduce un articulo a la lista\n2.-Mostrar los articulos en orden alfabético\n3.-Borrar un artículo\n4.-Salir");
			Scanner sc = new Scanner(System.in);
			int menu = sc.nextInt();
			sc.nextLine();

			switch (menu)
			{
			case 1:
				System.out.println("Añade un artículo");
				String artículo = sc.next();
				lista.add(artículo);
				break;
			case 2:
				System.out.println(lista);
				break;
			case 3:
				System.out.println("Introduce artículo a borrar");
				String borrar = sc.next();
				lista.remove(borrar);
				break;
			case 4:
				flag = true;
			
				break;

			default:
				break;
			}
		}
	}

}
